--------------------------------------------------------------------------------
-- Extensions
--------------------------------------------------------------------------------

DROP EXTENSION IF EXISTS pg_trgm;
DROP EXTENSION IF EXISTS tablefunc;

CREATE EXTENSION IF NOT EXISTS pg_trgm;       -- (similarity , show_Trgm,…, %, <->)
CREATE EXTENSION IF NOT EXISTS tablefunc;     -- (crosstab)

--------------------------------------------------------------------------------
-- Movies
--------------------------------------------------------------------------------

DROP TABLE IF EXISTS movies;

CREATE TABLE movies(
    id integer GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    title character varying(255),
    categories character varying(255),
    summary text,
    description text,
    tsv tsvector
);

CREATE TRIGGER tsv_update BEFORE INSERT OR UPDATE
ON movies FOR EACH ROW EXECUTE PROCEDURE
tsvector_update_trigger(tsv, 'pg_catalog.english', title, categories, summary, description);


--------------------------------------------------------------------------------
-- Logs
--------------------------------------------------------------------------------

DROP TABLE IF EXISTS logs;

CREATE TABLE logs(
    id integer GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    query text NOT NULL,
    timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

--------------------------------------------------------------------------------
-- Indexes
--------------------------------------------------------------------------------

CREATE INDEX tsv_index ON movies USING gin(tsv);
